
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;

/**
 * Servlet implementation class OpenStackMeetingsServlet
 */
@WebServlet("/OpenStackMeetingsServlet")
public class OpenStackMeetingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public OpenStackMeetingsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String sessionStatus = request.getParameter("session");
		String project = request.getParameter("project");
		String year = request.getParameter("year");
		
		if(session == null && (project ==null || year==null)) {
			if(project != null && year==null) {
				response.getWriter().println("Required parameter year is missing");
			}
			else if(project==null && year != null) {
				response.getWriter().println("Required parameter project is missing");
			}
			else  {
				response.getWriter().println("Required parameters year and project are missing");
			}
		}

		HttpSession firstSession = request.getSession();

		response.getWriter().println("History");
		

		if (firstSession.getAttribute("keepHist") != null) {
			if ((boolean) firstSession.getAttribute("keepHist") == true) {
				List<String> prevHistory = (List) firstSession.getAttribute("prevHistory");
				for (int i = 0; i < prevHistory.size(); i++) {
					response.getWriter().println(prevHistory.get(i));
				}
				String url = "http://eavesdrop.openstack.org/meetings/" + project + "/" + year;
				prevHistory.add(url);
				firstSession.setAttribute("prevHistory", prevHistory);

			}
		}
		if (sessionStatus != null) {
			if (sessionStatus.equals("start")) {
				List<String> prevHistory = new ArrayList<String>();
				boolean tracker = true;
				prevHistory.add("http://localhost:8080/assignment1/openstackmeetings?session=start");
				firstSession.setAttribute("keepHist", tracker);
				firstSession.setAttribute("prevHistory", prevHistory);
			}
		}
		
		if (sessionStatus != null) {
			if (sessionStatus.equals("end")) {
				List<String> prevHistory = (List) firstSession.getAttribute("prevHistory");
				for (int i = 0; i < prevHistory.size(); i++) {
					response.getWriter().println(prevHistory.get(i));
				}
				prevHistory = new ArrayList<String>();
				firstSession.setAttribute("keepHist", false);
				firstSession.setAttribute("prevHistory", prevHistory);
			}
		}
		response.getWriter().println("");
		response.getWriter().println("Data");

		if (project != null && year != null) {
			String url = "http://eavesdrop.openstack.org/meetings/" + project + "/" + year;
			Document webpage = Jsoup.connect(url).get();
			int counter = 0;
			Elements choices = webpage.getElementsByTag("a");
			
			for (Element choice : choices) {
				if(counter >= 5) {
				response.getWriter().println(choice.attr("href"));
				}
				counter++;
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
